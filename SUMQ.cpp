#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
const long long MOD=1000000007LL;
int main()
{
int t,p,q,r,pa,pb,pc;
int a[100001],b[100001],c[100001];
long long ans,sa[100001],sb[100001],sc[100001],sum;
cin>>t;
while(t--)
{
	ans=0LL;
	pa=pb=pc=0;
	cin>>p>>q>>r;
	for(int i=0;i<p;i++)
		scanf("%d",&a[i]);
	for(int i=0;i<q;i++)
		scanf("%d",&b[i]);
	for(int i=0;i<r;i++)
		scanf("%d",&c[i]);
	sort(a,a+p);
	sort(b,b+q);
	sort(c,c+r);
	sum=0LL;
	for(int i=0;i<p;i++){
		sa[i]=(sum+a[i])%MOD;sum=sa[i];}
	sum=0LL;
	for(int i=0;i<r;i++){
		sc[i]=(sum+c[i])%MOD;sum=sc[i];}

	for(int i=0;i<q;i++)
	{
		while(pa<p && a[pa]<=b[i])pa++;
		while(pc<r && c[pc]<=b[i])pc++;
		//cout<<pa<<" "<<pc<<endl;
		//cout<<sa[pa-1]<<" "<<sc[pc-1]<<endl;
		ans = (ans+(sa[pa-1]*sc[pc-1])%MOD)%MOD;
		long long x = (1LL * b[i] * ((((sa[pa-1]*pc)%MOD+ (sc[pc-1]*pa)%MOD)%MOD + (((1LL*pa)*pc)%MOD*b[i])%MOD)%MOD )%MOD)%MOD;
		ans = (ans + x)%MOD;
	}
	cout<<ans<<endl;
}
return 0;
}