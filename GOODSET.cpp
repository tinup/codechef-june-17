#include <iostream>
#include <cstdio>
using namespace std;
int main()
{
int t,n;
long long ans[101]={0LL};
ans[0]=1LL;
ans[1]=2LL;
for(int i=2;i<101;i++)
{
	bool found=false;
	long long x=ans[i-1]+1LL;
	while(!found)
	{
		retry:
		for(int j=0;j<i;j++)
		{
			for(int k=0;k<i;k++)
			{
				if(j==k)continue;
				if(ans[j]+ans[k]==x)
				{
					x++;
					found=false;
					goto retry;
				}
			}
		}
		found=true;
	}
	ans[i] = x;
}
cin>>t;
while(t--)
{
cin>>n;
for(int i=0;i<n;i++)
cout<<ans[i]<<" ";
cout<<endl;
}
return 0;
} 