#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
int main()
{
int t,n,pc,a[100001],mc;
long long ans,x,neg,ns,pos;
cin>>t;
while(t--)
{
cin>>n;
for(int i=0;i<n;i++)
cin>>a[i];
sort(a,a+n);
neg=pos=ns=0LL;
pc=mc=0;
int i=0;
for(i=n-1;i>=0;i--)
{
	if(a[i]<0)
	{
		neg+=a[i];
	}
	else
	{
		pos+=a[i];
		pc++;
	}
}
ans = pc*pos + neg;
for(i=n-1;i>=0;i--)
	if(a[i]<0)break;
for(;i>=0;i--)
{
	pc++;
	ns+=a[i];
	x = pc * (pos+ns) + (neg-ns);
	if(ans<x)ans=x;
}
cout<<ans<<endl;
}
return 0;
} 