#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
int t,n,k,ans;
vector<int> l(2500,0);
vector<vector<int> > a(2500);
cin>>t;
while(t--)
{
	ans=0;
	cin>>n>>k;
	for(int i=0;i<n;i++)
	{
		cin>>l[i];
		a[i].clear();
		a[i].resize(l[i]);
		for(int j=0;j<l[i];j++)
			scanf("%d",&a[i][j]);
	}
	
	vector<vector<bool> > p(n);
	for(int i=0;i<n;i++)
		p[i].resize(k,false);
	
	for(int i=0;i<n;i++)
		for(int j=0;j<l[i];j++)
			p[i][a[i][j]-1]=true;

	for(int i=0;i<n;i++)
		for(int j=i+1;j<n;j++)
		{
			int z=0;
			if(l[i]==k || l[j]==k)
				z=k;
			for(;z<k;z++)
				if(!p[i][z] && !p[j][z])
					break;
			if(z==k)
				ans++;
		}
	cout<<ans<<endl;
	// for(int i=0;i<n;i++){
	// 	for(int j=0;j<k;j++)
	// 		cout<<p[i][j]<<" ";
	// 	cout<<endl;
	// }


	// for(int i=0;i<n;i++)
	// {
	// 	for(int j=0;j<l[i];j++)
	// 		cout<<a[i][j]<<" ";
	// 	cout<<endl;
	// }
}

return 0;
}